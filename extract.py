import cv2
import argparse
from tqdm import tqdm

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--video_path')

args = parser.parse_args()
video_path = args.video_path

print(f"Loading video: {video_path}")
vcap = cv2.VideoCapture(video_path)
if not vcap.isOpened():
    print("Error: Could not open video.")
    exit

width = int(vcap.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(vcap.get(cv2.CAP_PROP_FRAME_HEIGHT))
fps = vcap.get(cv2.CAP_PROP_FPS)
frame_count = int(vcap.get(cv2.CAP_PROP_FRAME_COUNT))

print(f"{width}x{height} FPS:{fps} {frame_count} total frames")
vid_writer = cv2.VideoWriter("motion-ext-"+video_path, cv2.VideoWriter_fourcc(*"mp4v"), fps, (width, height))

progress_bar = tqdm(total=frame_count)

def extract_motion_and_save(vcap):
    previous_frame = 0
    frame_count = 0

    while True:
        ret, frame = vcap.read()

        # Check if the frame is read correctly
        if not ret:
            break

        if (frame_count > 0):
            sub = cv2.subtract(previous_frame, frame)
            vid_writer.write(sub)

        previous_frame = frame
        frame_count += 1
        progress_bar.update(1)

    vid_writer.release()
    print(f"Total frames extracted: {frame_count}")


extract_motion_and_save(vcap)
vcap.release()
progress_bar.close()